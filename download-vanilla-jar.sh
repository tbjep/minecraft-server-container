VERSION_MANIFEST_URL="https://launchermeta.mojang.com/mc/game/version_manifest.json"
VERSION_MANIFEST="$(curl "$VERSION_MANIFEST_URL")"

VERSION=$1
case "$VERSION" in
	"latest" | "latest-release") VERSION="release";;
	"latest-snapshot") VERSION="snapshot";;
esac

MANIFEST_FOR_VERSION_FILTER='((.latest[$ARGS.positional[0]]? // $ARGS.positional[0]) as $version | .versions[] | select (.id == $version) | .url) // error("Version not found: '"$VERSION"'")'

VERSION_DATA_URL="$(echo "$VERSION_MANIFEST" | jq -r --args "$MANIFEST_FOR_VERSION_FILTER" $VERSION)"
[ $? -eq 0 ] || exit 1

VERSION_DATA=$(curl "$VERSION_DATA_URL")

SERVER_JAR_URL_FILTER='.downloads.server.url'

SERVER_JAR_URL="$(echo "$VERSION_DATA" | jq -r $SERVER_JAR_URL_FILTER)"
curl -o minecraft.jar "$SERVER_JAR_URL"
