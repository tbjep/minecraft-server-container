INSTALLER_VERSIONS_URL="https://meta.fabricmc.net/v2/versions/installer"
INSTALLER_VERSIONS="$(curl "$INSTALLER_VERSIONS_URL")"

INSTALLER_VERSION="$1"

URL_FOR_VERSION_FILTER='$ARGS.positional[0] as $version | if $version == "latest" then .[0] else (map(select (.version == $ARGS.positional[0])) | .[0]) end | .url'

INSTALLER_URL="$(echo "$INSTALLER_VERSIONS" | jq -r --args "$URL_FOR_VERSION_FILTER" "$INSTALLER_VERSION")"
curl -o installer.jar "$INSTALLER_URL"
