ARG JAVA_VERSION=21
FROM openjdk:${JAVA_VERSION} AS base
ARG EULA=false

RUN useradd minecraft

RUN mkdir /data && chown minecraft:minecraft /data

USER minecraft

VOLUME /data

WORKDIR /data

FROM alpine as vanilla-build
ARG MC_VERSION=1.21.0

RUN apk add jq curl

COPY download-vanilla-jar.sh download-vanilla-jar.sh

RUN ./download-vanilla-jar.sh $MC_VERSION

FROM base as vanilla

COPY --from=vanilla-build minecraft.jar /minecraft.jar

ENV JAVA_ARGS=""

WORKDIR /data

EXPOSE 25565

ENTRYPOINT echo "eula=$EULA" > eula.txt && java $JAVA_ARGS -jar /minecraft.jar

FROM alpine as fabric-downloader
ARG FABRIC_INSTALLER_VERSION=latest

RUN apk add jq curl

COPY download-fabric-launcher.sh download-fabric-launcher.sh

RUN ./download-fabric-launcher.sh $FABRIC_INSTALLER_VERSION

FROM openjdk:${JAVA_VERSION} as fabric-build

COPY --from=fabric-downloader installer.jar /installer.jar

ARG MC_VERSION=1.21.0
RUN java -jar ./installer.jar server -mcversion $MC_VERSION

FROM base as fabric

COPY --from=vanilla-build minecraft.jar /minecraft.jar
COPY --from=fabric-build fabric-server-launch.jar /fabric-server-launch.jar
COPY --from=fabric-build libraries/ /libraries/

ENV JAVA_ARGS=""

WORKDIR /data

EXPOSE 25565

ENTRYPOINT echo "serverJar=/minecraft.jar" > fabric-server-launcher.properties && echo "eula=$EULA" > eula.txt && java $JAVA_ARGS -jar /fabric-server-launch.jar nogui
